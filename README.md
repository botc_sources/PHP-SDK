# BankOfTheCommons API integration for PHP

This repository is a PHP implementation to interact with the BankOfTheCommons API.

It provides interaction with many online payment providers like Bank Transfer, Spark,
Bank Card, Online Wallets, etc. with a secure authentication and authorization mechanism.

## The currently implemented services are:
* **Etherium:** pay and receive payments with ETH.
* **BankTransfer:** sends money with SEPA
* **Spark Card:** loadup/order your spark card.
* **Bitcoin:** pay and receive payments with BTC.
* **Faircoin:** pay and receive payments with FAIR.

## Installation
1. Get the code via clone
  * `git clone https://git.fairkom.net/faircoop/BankOfTheCommons/PHP-SDK.git`
2. Get via require in composer.json
  * `require: { "bankofthecommons/php-sdk": "dev-master" }`

## Basic sample
```
<?php

use TelepayApi\Core\UserCredentials;
use TelepayApi\TelepayClient;

$credentials = new UserCredentials(
    'YOUR_BOTC_ACCESS_KEY',
    'YOUR_BOTC_ACCESS_SECRET'
);

$tc = new BotC($credentials, 'https://api.bankofthecommons.coop');

print_r($tc->getSample()->make());
```



