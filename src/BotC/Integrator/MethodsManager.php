<?php
/**
 * Created by PhpStorm.
 * User: lluis
 * Date: 5/25/15
 * Time: 9:52 PM
 */

namespace BotC\Integrator;

use BotC\Integrator\Methods\BitcoinMethod;
use BotC\Integrator\Methods\FaircoinMethod;
use BotC\Integrator\Methods\SparkCardMethod;
use BotCLL\Core\Credentials;

class MethodsManager {
    public $bitcoin;
    public $faircoin;
    public $sparkCard;

    protected $url;

    public function __construct(Credentials $credentials, $url){
        $this->url = $url;
        $this->bitcoin = new BitcoinMethod($credentials, $url);
        $this->faircoin = new FaircoinMethod($credentials, $url);
        $this->sparkCard = new SparkCardMethod($credentials, $url);
    }
}