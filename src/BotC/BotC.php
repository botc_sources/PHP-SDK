<?php
namespace BotC;


use BotC\Account\AccountManager;
use BotC\Integrator\MethodsManager;
use BotC\Pub\PublicManager;
use BotC\User\WalletManager;
use BotCLL\Core\Credentials;


class BotC {
    public $methods;
    public $account;
    public $wallet;
    public $realtime;
    public $public;


    public function __construct(Credentials $credentials, $url){
        $this->methods = new MethodsManager($credentials, $url);
        $this->account = new AccountManager($credentials, $url);
//        $this->wallet = new WalletManager($credentials, $url);
        $this->public = new PublicManager($credentials, $url);
    }

}


